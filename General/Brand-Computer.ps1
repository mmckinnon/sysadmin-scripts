<#
Make sure you change the variables to match your business information

Modified code from:
https://weikingteh.wordpress.com/2015/01/13/powershell-script-to-insert-branding-oem-and-custom-wallpaper/
#>

$CoName = "Your Biz"
$CoPhone = "(206) 555-1212"
$CoHours = "8:00am to 5:00pm"
$CoUrl = "https://yoursite.com"

# Registry Changes
$strPath = "HKLM:\Software\Microsoft\Windows\CurrentVersion\OEMInformation"

Set-ItemProperty -Path $strPath -Name Manufacturer -Value "$CoName"
Set-ItemProperty -Path $strPath -Name SupportPhone -Value "$CoPhone"
Set-ItemProperty -Path $strPath -Name SupportHours -Value "$CoHours"
Set-ItemProperty -Path $strPath -Name SupportURL -Value $CoUrl


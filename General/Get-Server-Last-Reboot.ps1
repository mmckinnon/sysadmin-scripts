#Get Computer Details
$SERVERS = get-adcomputer -filter { OperatingSystem -like "Windows Server*" } -Property * | Select-Object Name

#Get System Boot Time and Host Name
ForEach ($SERVER in $SERVERS) {
	$Name = $SERVER.Name
	Write-Host "Getting System Details for" $Name -ForegroundColor Yellow
	$SystemInfo = systeminfo /s $Name /FO CSV | ConvertFrom-Csv
	Write-Host "Host Name:" $SystemInfo.'Host Name'
	Write-Host "System Boot Time:" $SystemInfo.'System Boot Time'

}


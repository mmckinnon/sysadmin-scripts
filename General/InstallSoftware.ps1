# Install Software

# Add .Net 3.5
Enable-WindowsOptionalFeature -Online -FeatureName "NetFx3"

# Install Chocolatey
iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex
choco feature enable -n=allowglobalconfirmation
choco feature enable -n=allowemptychecksums

# Install Chocolatey Software

choco install packages.config

#set Password for anydesk
cmd /c 'echo 18BealSt | "C:\Program Files (x86)\AnyDesk\anydesk.exe" --set-password'

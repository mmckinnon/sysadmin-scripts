$REG_PATH = "HKLM:\Software\Policies\Microsoft\Windows NT\CurrentVersion\Software Protection Platform"
$REG_NAME = "NoGenTicket"
$REG_VALUE = "1"

New-Item $REG_PATH -Force

New-ItemProperty -Path $REG_PATH -Name $REG_NAME -Value $REG_VALUE -PropertyType DWORD -Force

$bitness = get-itemproperty HKLM:\SOFTWARE\WOW6432Node\Microsoft\Office\16.0\Outlook -name Bitness

If($bitness -eq "x86") {
  #DO 32-BIT STUFF
  cd "\Program Files (x86)\Microsoft Office\Office16"

} else {
  #DO 64-BIT STUFF
  cd "\Program Files\Microsoft Office\Office16"
}

cscript //nologo ospp.vbs /inpkey:XQNVK-8JYDB-WJ9W3-YJ8YR-WFG99
cscript //nologo ospp.vbs /sethst:<KMS_SERVER>
cscript //nologo ospp.vbs /setprt:1688
cscript //nologo ospp.vbs /act
